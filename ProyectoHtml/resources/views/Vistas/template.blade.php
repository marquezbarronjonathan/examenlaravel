<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, maximum-scale=1.0"
    />
    <title>Plantilla inicial Materialize</title>

    <!-- CSS  -->
    <link
      href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"
    />
    <link
      href="css/materialize.css"
      type="text/css"
      rel="stylesheet"
      media="screen,projection"
    />
    <link
      href="css/style.css"
      type="text/css"
      rel="stylesheet"
      media="screen,projection"
    />
  </head>
  <body>


  <div class="navbar-fixed">
<nav class="grey darken-3">
    <div class="container">

      <a href="{{route('inicio')}}" class="brand-logo hide-on-small-only green-text text-darken-2">APP bloqueo android</a>
        <a href="{{route('inicio')}}" class="brand-logo show-on-small hide-on-large-only hide-on-med-only" style="font-size: 5vw;">APP bloqueo android</a>

      <ul class="right hide-on-med-and-down">
        
        <li><a href="{{route('contacto')}}">Contacto</a></li>
        
      </ul>

      <ul id="nav-mobile" class="sidenav">
        
        <li><a href="{{route('contacto')}}">Contacto</a></li>
        
      </ul>
      <ul class="right hide-on-med-and-down">
        
        <li><a href="{{route('caracteristicas')}}">Caracteristicas</a></li>
        
      </ul>

      <ul id="nav-mobile" class="sidenav">
        
        <li><a href="{{route('caracteristicas')}}">Caracteristicas</a></li>
        
      </ul>
      <ul class="right hide-on-med-and-down">
        
        <li><a href="{{route('inicio')}}">Inicio</a></li>
        
      </ul>

      <ul id="nav-mobile" class="sidenav">
        
        <li><a href="{{route('inicio')}}">Inicio</a></li>
        
      </ul>


      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>

    </div>


  </nav>
  </div>

 
@yield('contenido')


    

  <footer class="page-footer black darken-4">
    <div class="container">
      <div class="row">
        <div class="col l6 m6 s12">
          <h5>Contacto</h5>
          <br>
          <pr">Plaza IT oficina 3 sección diamante, 5 de mayo, Lagos de Moreno,Jalisco, México soporte@android-app.com</p>
        </div>
        <div class="col l6 s12 center-align">
          <h5>Suscribete</h5>
          <i class="material-icons">facebook</i> <i class="material-icons">rss_feed</i>
          
        </div>
      
      </div>
    </div>
    <div class="footer-copyright col l12 m6 s12 grey darken-4">
      <div class="center container"> <i class="material-icons">copyright</i> Todos los derechos reservados 2020.</div>
     

  </footer>


    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="js/materialize.js"></script>
    <script src="js/init.js"></script>
  </body>
</html>
