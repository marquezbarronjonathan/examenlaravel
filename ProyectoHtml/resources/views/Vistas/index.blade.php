@extends('Vistas.template')

@section('contenido')

@include('Parcial.gestionremota')
<div class="row">
    
    <div class="col l2">
    

    </div>
    <div class="col l8 s12">
      <div class="row">
        <div class="col l6 s12">
          <h4 class="green-text text-darken-2">Funcion del navegador</h4>
          
          <p align="justify" class="black-text"><font class="green-text 
            text-darken-2">Kiosk Browser </font> tiene un amplio conjunto de funciones. Puede probar estas funciones durante 5 días simplemente instalando, se requiere una licencia para uso personal y comercial. </p>
          <div class="col l4 s5">
            
            <img src="img/google-play-badge.png" class="responsive-img" alt="">

          </div>
          <div class="col l8 "></div>
         

        </div>
        <div class="col l6 s12 center-align">
          <img src="img/candado.png" class="responsive-img " alt="">


        </div>

      </div>
    

    </div>
    <div class="col l2">
      

    </div>


  </div>
  
  <div class="row">
  
    <div class="col l2">
    

    </div>
    <div class="col l8 s12">
      <div class="row">
        <div class="col l6 s12 center-align">
         <img src="img/candado2.png" class="responsive-img" alt="">

        </div>
        <div class="col l6 s12">
          <h4 class="green-text text-darken-2">Funcion del lanzador</h4>
          
          <p align="justify" class="black-text"> <font class="orange-text text-darken-4">Kiosk Launcher</font> tiene un amplio conjunto de funciones. Puede probar estas funciones durante 5 días simplemente instalando, se requiere una licencia para uso personal y comercial. </p>
      
          <input class="btn orange darken-4" type="button" value="Instalar">

        </div>

      </div>
    

    </div>
    <div class="col l2">
      

    </div>


  </div>
  <div class="row">
  
    <div class="col l2">
    

    </div>
    <div class="col l8 s12">
      <div class="row">
        <div class="col l6 s12 center-align">
          <h4 class="green-text text-darken-2">Opiniones de clientes</h4>

          <blockquote style="border-color: green;">
            <p align=justify> Cuando comenzamos a trabajar con esta aplicación, tenía una curva de aprendizaje bastante empinada. Estos chicos han estado trabajando diligentemente en la construcción de su documentación, escuchando a sus clientes y agregando correcciones / características adicionales; y ha valido la pena! Esta aplicación vale cada centavo si está buscando usarla comercialmente como lo hemos hecho nosotros.
              <br>
              <br>
              <font class="orange-text text-darken-4">Gunther Vinson, TowMate LLC</font></p>
        </blockquote>


        </div>
        <div class="col l6 s12">
         <br>
         <br>
         <br>
          <blockquote style="border-color: green;">
            
            <p align=justify> Gran producto, soporte increíble . Usamos este producto en aproximadamente 40 ubicaciones en nuestra empresa y funciona perfectamente todo el día, todos los días.
              <br>
              <br>
              <font class="orange-text text-darken-4">David Higginson</font></p>
        </blockquote >
        <br>
        <blockquote style="border-color: green;">
            
          <p align=justify> Perfecto , utilícelo para nuestro quiosco de reserva de canchas de tenis con una aplicación web Google Apps Script. Funciona perfectamente.
            <br>
            <br>
            <font class="orange-text text-darken-4">Serge Gravelle</font></p>
      </blockquote>
         

        </div>

      </div>
    

    </div>
    <div class="col l2">
      

    </div>


  </div>
@endsection