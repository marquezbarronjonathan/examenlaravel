@extends('Vistas.template')

@section('contenido')

@include('Parcial.gestionremota')
<div class="row">
    
      <div class="col l2">
      

      </div>
      <div class="col l8 s12">
        <div class="row">
          <div class="col l6 s12">
            <h4 class="black-text text-darken-2">Caracteristicas</h4>
            
            <p align="justify" class="black-text">La administración remota está repleta de excelentes funciones para ayudarlo a administrar y mantener sus dispositivos.
                <br>
                <br>
                Perfiles de configuración <font class="green darken-2"><font class="white-text">Navegador</font></font> <font class="orange darken-4"><font class="white-text">Lanzador</font></font>
            <br> 
            <br>
            Envíe la misma configuración a grupos de dispositivos
        <br>
        <br>
        Acciones de dispositivo push <font class="green darken-2"><font class="white-text">Navegador</font></font> <font class="orange darken-4"><font class="white-text">Lanzador</font></font>
    <br>
    <br>
    Ejecute acciones inmediatas contra cada dispositivo, como reiniciar la aplicación, reiniciar *, etc.
<br>
<br>
Actualizaciones <font class="green darken-2"><font class="white-text">Navegador</font></font> <font class="orange darken-4"><font class="white-text">Lanzador</font></font>
<br>
<br>
Instale las actualizaciones del Kiosk Browser / Launcher de forma remotasin visitar sus dispositivos *
</p>
           

          </div>
          <div class="col l6 s12 center-align">
            <img src="img/caracteristicas2.png" class="responsive-img " alt="">


          </div>

        </div>
      

      </div>
      <div class="col l2">
        

      </div>


    </div>
@endsection