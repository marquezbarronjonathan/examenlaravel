@extends('Vistas.template')

@section('contenido')
<div class="row grey darken-3">
    
    <div class="col l2">
    

    </div>
    <div class="col l8 s12 center-align">
     
      <h4 class="white-text text-darken-2 center-align">Ponerse en contacto</h4>
      <p align="justify" class="white-text center-align" >¿Necesitas contactarnos? Inicie un chat o envíenos un correo electrónico. </p>
      <input class="btn white darken-2 green-text text-darken-2" type="button" value="HABLA CON NOSOTROS">
      <br>
      <br>

    </div>
    <div class="col l2">
      

    </div>


  </div>
  <div class="row">
  
    <div class="col l2">
    

    </div>
    <div class="col l8 s12">
      <div class="row">
      <div class="col l12 s12">
          <div class="card-panel ">
              <h5 class="center-align">Envianos un correo electrónico</h5>
              <br>
              <p align="justify" class="black-text center-align" >Su correo electrónico se enviará automáticamente a través de nuestro sistema de chat en vivo, se le enviará un correo electrónico cuando respondamos. </p>

          <br>
          <input class="white" type="text" value="Correo">
          <br>
          <input class="white" type="text" value="Mensaje">
          <button type="submit" class="btn green darken-2 white-text text-darken-2"> Enviar <i class="material-icons">send</i></button>

         

          </div>
      </div>
    

    </div>
    </div>
    <div class="col l2">
      

    </div>


  </div>
@endsection