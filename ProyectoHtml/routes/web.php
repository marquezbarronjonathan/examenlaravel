<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();

Route::get('/',['as' => 'inicio', function () {
    return view('Vistas.index');
}]);

//Auth::routes();

Route::get('/Caracteristicas', function () {
    return view('Caracteristicas.caracteristicas');
})->name('caracteristicas');



Route::get('/Contacto', 'ControllerRutas@llamarcontacto')->name('contacto');
